async def load_filevault_enabled(hub):
    """
    Find out whether FileVault is enabled, via fdesetup.
    """
    hub.corn.CORN.filevault = (
        "FileVault is On."
        == (await hub.exec.cmd.run(["/usr/bin/fdesetup", "status"]))["stdout"].strip()
    )
