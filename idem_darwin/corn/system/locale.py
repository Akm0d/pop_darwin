import datetime
import dateutil.tz as tz
import locale
import sys


async def load_info(hub):
    """
    Provides
        defaultlanguage
        defaultencoding
    """
    try:
        (
            hub.corn.CORN.locale.defaultlanguage,
            hub.corn.CORN.locale.defaultencoding,
        ) = locale.getdefaultlocale()
    except Exception:  # pylint: disable=broad-except
        # locale.getdefaultlocale can ValueError!! Catch anything else it
        # might do, per #2205
        hub.corn.CORN.locale.defaultlanguage = "unknown"
        hub.corn.CORN.locale.defaultencoding = "unknown"
    hub.corn.CORN.locale.detectedencoding = sys.getdefaultencoding() or "ascii"

    hub.corn.CORN.locale.timezone = datetime.datetime.now(tz.tzlocal()).tzname()
