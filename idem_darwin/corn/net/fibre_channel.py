import aiofiles
import glob


async def load_wwn(hub):
    """
    Return Fibre Channel port WWNs from a Linux host.
    """
    ret = []
    for fc_file in glob.glob("/sys/class/fc_host/*/port_name"):
        async with aiofiles.open(fc_file, "r") as _wwn:
            content = await _wwn.read()
            for line in content.splitlines():
                ret.append(line.rstrip()[2:])
    if ret:
        hub.corn.CORN.fc_wwn = ret
