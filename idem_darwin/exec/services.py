import aiofiles
import logging
import os
import plistlib
import xml.parsers.expat
from typing import Any, Dict

log = logging.getLogger(__name__)

__virtualname__ = "srv"


def __init__(hub):
    hub.exec.srv.CACHED_SERVICES = {}


async def read_plist_file(hub, root: str, file_name: str) -> Dict[str, Any]:
    """
    :param root: The root path of the plist file
    :param file_name: The name of the plist file
    :return:  An empty dictionary if the plist file was invalid, otherwise, a dictionary with plist data
    """
    file_path = os.path.join(root, file_name)
    log.debug("read_plist: Gathering service info for {}".format(file_path))

    # Must be a plist file
    if not file_path.lower().endswith(".plist"):
        log.debug("read_plist: Not a plist file: {}".format(file_path))
        return {}

    # ignore broken symlinks
    if not os.path.exists(os.path.realpath(file_path)):
        log.warning("read_plist: Ignoring broken symlink: {}".format(file_path))
        return {}

    try:
        async with aiofiles.open(file_path, "rb") as handle:
            plist = plistlib.load(handle.read())

    except plistlib.InvalidFileException:
        # Raised in python3 if the file is not XML.
        # There's nothing we can do; move on to the next one.
        log.warning(
            'read_plist: Unable to parse "{}" as it is invalid XML: InvalidFileException.'.format(
                file_path
            )
        )
        return {}

    except xml.parsers.expat.ExpatError:
        # Raised by py2 for all errors.
        # Raised by py3 if the file is XML, but with errors.
        # There's an error in the XML, so move on.
        log.warning(
            'read_plist: Unable to parse "{}" as it is invalid XML: xml.parsers.expat.ExpatError.'.format(
                file_path
            )
        )
        return {}

    # Use the system provided plutil program to attempt
    # conversion from binary.
    try:
        plist_xml = (
            await hub.exec.cmd.run(
                ["/usr/bin/plutil", "-convert", "xml1", "-o", "-", "--", file_path]
            )
        )["stdout"]
        plist = plistlib.readPlistFromString(plist_xml)
    except xml.parsers.expat.ExpatError:
        # There's still an error in the XML, so move on.
        log.warning(
            'read_plist: Unable to parse "{}" as it is invalid XML: xml.parsers.expat.ExpatError.'.format(
                file_path
            )
        )
        return {}

    if "Label" not in plist:
        # not all launchd plists contain a Label key
        log.debug(
            "read_plist: Service does not contain a Label key. Skipping {}.".format(
                file_path
            )
        )
        return {}

    return {
        "file_name": file_name,
        "file_path": file_path,
        "plist": plist,
    }


async def available_services(hub, refresh: bool = False) -> Dict[str, Any]:
    """
    This is a helper function for getting the available macOS services.

    The strategy is to look through the known system locations for
    launchd plist files, parse them, and use their information for
    populating the list of services. Services can run without a plist
    file present, but normally services which have an automated startup
    will have a plist file, so this is a minor compromise.
    """
    if hub.exec.srv.CACHED_SERVICES and not refresh:
        log.debug("Found context for available services.")
        return hub.exec.srv.CACHED_SERVICES

    launchd_paths = {
        "/Library/LaunchAgents",
        "/Library/LaunchDaemons",
        "/System/Library/LaunchAgents",
        "/System/Library/LaunchDaemons",
    }

    agent_path = "/Users/{}/Library/LaunchAgents"
    launchd_paths.update(
        {
            agent_path.format(user)
            for user in os.listdir("/Users/")
            if os.path.isdir(agent_path.format(user))
        }
    )

    result = {}
    for launch_dir in launchd_paths:
        for root, dirs, files in os.walk(launch_dir):
            for file_name in files:
                data = await hub.exec.srv.read_plist_file(root, file_name)
                if data:
                    result[data["plist"]["Label"].lower()] = data

    hub.exec.srv.CACHED_SERVICES = result
    return hub.exec.srv.CACHED_SERVICES
