**********
POP_DARWIN
**********
**Grains, execution modules, and state modules common to all darwin systems**

INSTALLATION
============


Pip install corn::

    pip install -e git+https://gitlab.com/saltstack/pop/corn.git#egg=chunkies

Clone the `pop_darwin` repo and install with pip::

    git clone https://gitlab.com/saltstack/pop/pop_darwin.git
    pip install -e pop_darwin

EXECUTION
=========
After installation the `corn` command should now be available

TESTING
=======
install `requirements-test.txt` with pip and run pytest::

    pip install -r pop_darwin/requirements-test.txt
    pytest pop_darwin/tests

VERTICAL APP-MERGING
====================
Instructions for extending pop-darwin into an OS-specific pop project

Install pop::

    pip install --upgrade pop

Create a new directory for the project::

    mkdir pop_{specific_darwin_os}
    cd pop_{specific_darwin_os}


Use `pop-seed` to generate the structure of a project that extends `corn` and `idem`::

    pop-seed -t v pop_{specific_darwin_os} -d corn exec states

* "-t v" specifies that this is a vertically app-merged project
*  "-d corn exec states" says that we want to implement the dynamic names of "corn", "exec", and "states"

Add "pop_darwin" to the requirements.txt::

    echo "pop_darwin @ git+https://gitlab.com/saltstack/pop/pop_darwin.git" >> requirements.txt

And that's it!  Go to town making corn, execution modules, and state modules specific to your darwin os.
Follow the conventions you see in pop_darwin.

For information about running idem states and execution modules check out
https://idem.readthedocs.io
